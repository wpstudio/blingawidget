<?php
/*
Plugin Name: Blingtasitic Widget Wrapper
Plugin URI: http://blingorama.com 
Description: Wrap widgets like in 1999
Version: 0.5
Author: Blingmaster Donna
Author URI: http://blingorama.com 
License: GPLv2
Bitbucket Plugin URI: https://bitbucket.org/wpstudio/blingawidget
*/

if(!class_exists('BlingAWidget')){
	class BlingAWidget {
	
	public function __construct(){
		global $wp_version;
		if(version_compare($wp_version, '3.1.1', '<')){
			wp_die(__('BlingAWidget requires WordPress version 3.1.1 or newer, please upgrade your WordPress site or pay BlingMaster Donna $1,000 to upgrade it for you.', 'blingawidget'));
		}
		if(version_compare(phpversion(), '5.2.4', '<')){
			wp_die(__('BlingAWidget needs a newer PHP version.  (higher than 5.2.4)', 'blingawidget'));
		}
		
		add_filter('widget_form_callback', array($this, 'Form'), 10, 2);
		add_filter('widget_update_callback', array($this, 'Update'), 10, 2);
		add_filter('dynamic_sidebar_params', array($this, 'Apply'));
		
		add_filter('plugin_row_meta', array($this, 'FilterPluginRowMeta'), 10, 2); 
	}
	
	
	function Form($instance, $widget){
		if(!isset($instance['bawclass'])){
			$instance['bawclass'] = null;
		}
		?>
		<p>
			<label for="widget-<?php echo $widget->id_base; ?>-<?php echo $widget->number; ?>-bawclass">BlingAWidget Class:</label>
			<input type="text" name="widget-<?php echo $widget->id_base; ?>[<?php echo $widget->number; ?>][bawclass]" id="widget-<?php echo $widget->id_base; ?>-<?php echo $widget->number; ?>-bawclass" class="widefat" value="<?php echo $instance['bawclass']; ?>" />
			
		</p>
		<?php
		return $instance;
	}
	
	function Update($instance, $new_instance){
		$instance['bawclass'] = $new_instance['bawclass'];
		return $instance;
	}
	
	function Apply($params){
		global $wp_registered_widgets;
		$widget_id = $params[0]['widget_id'];
		$widget = $wp_registered_widgets[$widget_id];
		
		if(!($widgetlogicfix = $widget['callback'][0]->option_name)) $widgetlogicfix = $widget['callback_wl_redirect'][0]->option_name;
		
		$option_name = get_option($widgetlogicfix);
		
		$number = $widget['params'][0]['number'];
		
		if(isset($option_name[$number]['bawclass']) && !empty($option_name[$number]['bawclass'])){
			$params[0]['before_widget'] = preg_replace('/class="/', "class=\"{$option_name[$number]['bawclass']} ", $params[0]['before_widget'], 1);	
			
		}
		
		return $params;
	}
	
	public function FilterPluginRowMeta($links, $file){
		$plugin = plugin_basename(__FILE__);
		if($file == $plugin) return array_merge($links, array('<a target="_blank" href="http://blingorama.com">Dance the night away!</a>'));		
		return $links;
	}
	
	}
} else {
	exit('Class BlingAWidget already declared!');
}

$objBlingAWidget = new BlingAWidget();